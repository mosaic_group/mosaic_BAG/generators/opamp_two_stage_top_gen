#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from two_stage_opamp_nmos_gen.params import two_stage_opamp_nmos_layout_params
from cmfb_gen.params import cmfb_layout_params
from mux_2to1_gen.params import mux_2to1_layout_params
from mux_sw_gen.params import mux_sw_layout_params
from resistor_diff_gen.params import resistor_diff_layout_params
from pseudo_resistor_ctrl_gen.params import pseudo_resistor_ctrl_layout_params
from half_vdd_gen.params import half_vdd_layout_params
from cap_mom_array_gen.params import cap_mom_array_layout_params


@dataclass
class opamp_two_stage_top_layout_params(LayoutParamsBase):
    """
    Parameter class for opamp_two_stage_top_gen

    Args:
    ----
    inverter_params : inverter2_params
        Parameters for inverter2 sub-generators

    tgate_params : tgate_params
        Parameters for tgate sub-generators

    show_pins : bool
        True to create pin labels
    """

    opamp_params: two_stage_opamp_nmos_layout_params
    cmfb_params: cmfb_layout_params
    mux_2to1_params: mux_2to1_layout_params
    mux_sw_params: mux_sw_layout_params
    dres_params: resistor_diff_layout_params
    pres_params: pseudo_resistor_ctrl_layout_params
    hvdd_params: half_vdd_layout_params
    cap_params: cap_mom_array_layout_params
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> opamp_two_stage_top_layout_params:
        return opamp_two_stage_top_layout_params(
            opamp_params=two_stage_opamp_nmos_layout_params.finfet_defaults(min_lch),
            cmfb_params=cmfb_layout_params.finfet_defaults(min_lch),
            mux_2to1_params=mux_2to1_layout_params.finfet_defaults(min_lch),
            mux_sw_params=mux_sw_layout_params.finfet_defaults(min_lch),
            dres_params=resistor_diff_layout_params.finfet_defaults(min_lch),
            pres_params=pseudo_resistor_ctrl_layout_params.finfet_defaults(min_lch),
            hvdd_params=half_vdd_layout_params.finfet_defaults(min_lch),
            cap_params=cap_mom_array_layout_params.finfet_defaults(min_lch),
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> opamp_two_stage_top_layout_params:
        return opamp_two_stage_top_layout_params(
            opamp_params=two_stage_opamp_nmos_layout_params.planar_defaults(min_lch),
            cmfb_params=cmfb_layout_params.planar_defaults(min_lch),
            mux_2to1_params=mux_2to1_layout_params.planar_defaults(min_lch),
            mux_sw_params=mux_sw_layout_params.planar_defaults(min_lch),
            dres_params=resistor_diff_layout_params.planar_defaults(min_lch),
            pres_params=pseudo_resistor_ctrl_layout_params.planar_defaults(min_lch),
            hvdd_params=half_vdd_layout_params.planar_defaults(min_lch),
            cap_params=cap_mom_array_layout_params.planar_defaults(min_lch),
            show_pins=True,
        )


@dataclass
class opamp_two_stage_top_params(GeneratorParamsBase):
    layout_parameters: opamp_two_stage_top_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> opamp_two_stage_top_params:
        return opamp_two_stage_top_params(
            layout_parameters=opamp_two_stage_top_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
