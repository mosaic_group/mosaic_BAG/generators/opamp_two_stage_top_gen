"""
opamp_two_stage_top
========

"""

from typing import *

from bag.layout.template import TemplateBase
from sal.simulation.measurement_base import MeasurementBase
from sal.params_base import GeneratorParamsBase
from sal.design_base import DesignBase

from .params import opamp_two_stage_top_params
from .layout import layout as opamp_two_stage_top_layout


class design(DesignBase):
    def __init__(self):
        super().__init__()
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self) -> str:
        return "opamp_two_stage_top_gen"

    @classmethod
    def layout_generator_class(cls) -> Optional[Type[TemplateBase]]:
        """Return the layout generator class"""
        return opamp_two_stage_top_layout

    @classmethod
    def parameter_class(cls) -> Type[GeneratorParamsBase]:
        """Return the parameter class"""
        return opamp_two_stage_top_params

    @classmethod
    def measurement_classes(cls) -> List[Type[MeasurementBase]]:
        """Return a list of measurement classes"""
        return []

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> opamp_two_stage_top_params:
        return self._params

    @params.setter
    def params(self, val: opamp_two_stage_top_params):
        self._params = val
