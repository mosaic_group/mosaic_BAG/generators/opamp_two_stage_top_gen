import os
from typing import *
from bag.design import Module

from .params import opamp_two_stage_top_layout_params

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/opamp_two_stage_top_templates',
                         'netlist_info', 'opamp_two_stage_top.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library opamp_two_stage_top_templates cell opamp_two_stage_top.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            opamp_sch_params='two_stage_opamp schematic parameter dictionary',
            cmfb_sch_params='cmfb schematic parameter dictionary',
            cap_sch_params='cap schematic parameter dictionary',
            mux_2to1_sch_params='mux_2to1 schematic parameter dictionary',
            mux_sw_sch_params='mux_sw schematic parameter dictionary',
            hvdd_sch_params='half vdd schematic parameter dictionary',
            dres_sch_params='resistor-diff schematic parameter dictionary',
            pres_sch_params='pseudo resistor schematic parameter dictionary',
        )

    def design(self,
               opamp_sch_params: Dict[str, Any],
               cmfb_sch_params: Dict[str, Any],
               cap_sch_params: Dict[str, Any],
               mux_2to1_sch_params: Dict[str, Any],
               mux_sw_sch_params: Dict[str, Any],
               hvdd_sch_params: Dict[str, Any],
               dres_sch_params: Dict[str, Any],
               pres_sch_params: Dict[str, Any]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """
        self.instances['AMP_Core'].design(**opamp_sch_params)
        self.instances['CMFB'].design(**cmfb_sch_params)
        self.instances['CCP'].design(**cap_sch_params)
        self.instances['CCN'].design(**cap_sch_params)
        self.instances['MUX0'].design(**mux_2to1_sch_params)
        self.instances['MUX1'].design(**mux_2to1_sch_params)
        self.instances['MUX2'].design(**mux_sw_sch_params)
        self.instances['DUM_MUX'].design(**mux_sw_sch_params)
        self.instances['HVDD0'].design(**hvdd_sch_params)
        self.instances['HVDD1'].design(**hvdd_sch_params)
        self.instances['Res_diff1'].design(**dres_sch_params)
        self.instances['Res_diff2'].design(**dres_sch_params)
        self.instances['PRes'].design(**pres_sch_params)
        self.instances['DUM_PRES'].design(**pres_sch_params)
