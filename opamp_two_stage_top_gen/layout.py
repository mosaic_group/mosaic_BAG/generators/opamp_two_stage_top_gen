import copy
from typing import *

from bag.layout.core import BBox
from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase

from sal.routing_grid import RoutingGridHelper
from sal.via import ViaDirection

from two_stage_opamp_nmos_gen.layout import two_stage_opamp_nmos
from cmfb_gen.layout import cmfb
from mux_2to1_gen.layout import mux_2to1
from mux_sw_gen.layout import mux_sw
from resistor_diff_gen.layout import resistor_diff
from pseudo_resistor_ctrl_gen.layout import pseudo_resistor_ctrl
from half_vdd_gen.layout import half_vdd
from cap_mom_array_gen.layout import cap_mom_array

from .params import opamp_two_stage_top_layout_params


class layout(TemplateBase):
    """
    Top opamp including amp core, CMFB, Multiplexers, Half_VDD,
    Pseudo Resistors, Resistors and Compensation Capacitors.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='opamp_two_stage_top_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        params: opamp_two_stage_top_layout_params = self.params['params'].copy()

        # create layout masters for subcells we will add later
        opamp_master = self.new_template(temp_cls=two_stage_opamp_nmos, params=dict(params=params.opamp_params))
        cmfb_master = self.new_template(temp_cls=cmfb, params=dict(params=params.cmfb_params))
        mux_2to1_master = self.new_template(temp_cls=mux_2to1, params=dict(params=params.mux_2to1_params))
        mux_sw_master = self.new_template(temp_cls=mux_sw, params=dict(params=params.mux_sw_params))
        dres_master = self.new_template(temp_cls=resistor_diff, params=dict(params=params.dres_params))
        pres_master = self.new_template(temp_cls=pseudo_resistor_ctrl, params=dict(params=params.pres_params))
        hvdd_master = self.new_template(temp_cls=half_vdd, params=dict(params=params.hvdd_params))
        cap_master = self.new_template(temp_cls=cap_mom_array, params=dict(params=params.cap_params))

        # set grid and layers
        hm_layer = 4
        vm_layer = hm_layer + 1
        bot_layer = hm_layer - 1
        top_layer = vm_layer + 1
        grid = self.grid

        pitch_hm = self.grid.get_track_pitch(hm_layer, unit_mode=True)
        pitch_vm = self.grid.get_track_pitch(vm_layer, unit_mode=True)

        # Get Subcells width
        x_cmfb = cmfb_master.bound_box.right_unit
        x_amp = opamp_master.bound_box.right_unit
        x_dRes = dres_master.bound_box.right_unit
        x_Cap = cap_master.bound_box.right_unit
        x_HVDD = hvdd_master.bound_box.right_unit
        x_MUX_2to1 = mux_2to1_master.bound_box.right_unit
        x_MUX_SW = mux_sw_master.bound_box.right_unit
        x_PRes = pres_master.bound_box.right_unit

        # Horizontal spacing between blocks
        x_space = 5 * pitch_vm
        # Set Top block center point
        x_zero = max(x_amp // 2, 0.5 * x_space + x_MUX_2to1, x_cmfb // 2) + \
                 2 * x_space + \
                 max(x_dRes, x_Cap, x_HVDD, x_PRes + x_MUX_SW)
        # Set Column width based on max width of Subcells in that column
        x_col_1 = max(x_MUX_2to1 + x_space // 2, x_amp // 2, x_cmfb // 2) + 2 * x_space
        x_col_2 = x_col_1 + max(x_HVDD, x_dRes, x_Cap, x_PRes + x_MUX_SW + x_space // 2)
        col_list = [x_zero, x_col_1, x_col_2]
        # Check if the column's coordinates are not on grids, modify that
        for col in col_list:
            if col % pitch_vm != 0:
                col = ((col // pitch_vm) + 1) * pitch_vm

        # Get Subcells Height
        y_cmfb = cmfb_master.bound_box.top_unit
        y_amp = opamp_master.bound_box.top_unit
        y_dRes = dres_master.bound_box.top_unit
        y_Cap = cap_master.bound_box.top_unit
        y_MUX_SW = mux_sw_master.bound_box.top_unit
        y_PRes = pres_master.bound_box.top_unit
        y_MUX_2to1 = mux_2to1_master.bound_box.top_unit
        y_HVDD = hvdd_master.bound_box.top_unit
        # Vertical spacing between blocks
        y_space = 12 * pitch_hm
        # Set Height of each row based on Subcells in each
        y_row_0 = 0
        y_row_1 = max(y_HVDD, y_MUX_2to1) + y_space
        y_row_2 = y_row_1 + max(y_cmfb, y_MUX_SW, y_PRes) + y_space
        y_row_3 = y_row_2 + max(y_amp, y_dRes, y_Cap) + y_space
        row_list = [y_row_0, y_row_1, y_row_2, y_row_3]
        # # Check if the row's coordinates are not on grids, modify that
        for row in row_list:
            if row % pitch_hm != 0:
                row = ((row // pitch_hm) + 1) * pitch_hm

        # add subcell instances
        CMFB_inst = self.add_instance(cmfb_master, 'CMFB', loc=(
            ((col_list[0] - (x_cmfb // 2)) // (pitch_hm / 2)) * (pitch_hm / 2), row_list[1]),
                                      unit_mode=True)  # it should be multipe of 32
        Amp_inst = self.add_instance(opamp_master, 'AMP_Core', loc=(
            ((col_list[0] - (x_amp // 2)) // (pitch_hm / 2)) * (pitch_hm / 2), row_list[2]),
                                     unit_mode=True)  # to do

        dRes_inst_r = self.add_instance(dres_master, 'Res_diff0',
                                        loc=(col_list[0] + col_list[1], row_list[2] + (pitch_hm * 10)),
                                        unit_mode=True)  # to do
        dRes_inst_l = self.add_instance(dres_master, 'Res_diff1',
                                        loc=(col_list[0] - col_list[1], row_list[2] + (pitch_hm * 10)),
                                        unit_mode=True, orient='MY')  # to do

        MUX_inst_r = self.add_instance(mux_2to1_master, 'MUX0',
                                       loc=(col_list[0] + x_space // 2, row_list[0]),
                                       unit_mode=True)
        MUX_inst_l = self.add_instance(mux_2to1_master, 'MUX1',
                                       loc=(col_list[0] - x_space // 2, row_list[0]),
                                       unit_mode=True,
                                       orient='MY')

        HVDD_inst_r = self.add_instance(hvdd_master, 'HVDD1',
                                        loc=(col_list[0] + col_list[1], row_list[0]),
                                        unit_mode=True)
        HVDD_inst_l = self.add_instance(hvdd_master, 'HVDD0',
                                        loc=(col_list[0] - col_list[1], row_list[0]),
                                        unit_mode=True,
                                        orient='MY')
        # Dummy instances for keeping the layout symetrical
        MUX_SW_inst_r = self.add_instance(mux_sw_master, 'DUM_MUX',
                                          loc=(col_list[0] + col_list[1] + x_PRes, row_list[1]),
                                          unit_mode=True)  # to do
        Ps_Res_inst_r = self.add_instance(pres_master, 'DUM_PRES',
                                          loc=(col_list[0] + col_list[1], row_list[1]),
                                          unit_mode=True)

        MUX_SW_inst_l = self.add_instance(mux_sw_master, 'MUX2',
                                          loc=(col_list[0] - col_list[1] - x_PRes, row_list[1]),  # to do
                                          unit_mode=True, orient='MY')
        Ps_Res_inst_l = self.add_instance(pres_master, 'PRes',
                                          loc=(col_list[0] - col_list[1], row_list[1]),
                                          unit_mode=True, orient='MY')

        Cap_inst_r = self.add_instance(cap_master, 'CCN',
                                       loc=(col_list[0] + col_list[1], row_list[2]),
                                       unit_mode=True)
        Cap_inst_l = self.add_instance(cap_master, 'CCP',
                                       loc=(col_list[0] - col_list[1], row_list[2]),
                                       unit_mode=True, orient='MY')

        # get input/output wires from Subcells
        Amp_outn_warr = Amp_inst.get_all_port_pins('outn')[0]
        Amp_outp_warr = Amp_inst.get_all_port_pins('outp')[0]
        Amp_xp_warrs = Amp_inst.get_all_port_pins('xp')[0]
        Amp_xn_warrs = Amp_inst.get_all_port_pins('xn')[0]
        Amp_inp_warrs = Amp_inst.get_all_port_pins('inp')[0]
        Amp_inn_warrs = Amp_inst.get_all_port_pins('inn')[0]
        Amp_cmbias_warrs = Amp_inst.get_all_port_pins('cmbias')[0]
        Amp_ref_in_warrs = Amp_inst.get_all_port_pins('ref_in')[0]
        Amp_cmfb_ibias_warrs = Amp_inst.get_all_port_pins('cmfb_ibias')[0]
        Amp_VDD_warrs = Amp_inst.get_all_port_pins('VDD')[0]
        Amp_VSS_warrs = Amp_inst.get_all_port_pins('VSS')[0]

        Cap_r_plus_warr = Cap_inst_r.get_all_port_pins('plus')[0]
        Cap_r_minus_warrs = Cap_inst_r.get_all_port_pins('minus')[0]
        Cap_l_plus_warr = Cap_inst_l.get_all_port_pins('plus')[0]
        Cap_l_minus_warrs = Cap_inst_l.get_all_port_pins('minus')[0]
        Cap_rt_VDD_warrs = Cap_inst_r.get_all_port_pins('VDD')[params.cap_params.ny]  # 4
        Cap_rb_VDD_warrs = Cap_inst_r.get_all_port_pins('VDD')[params.cap_params.ny + 1]  # 5
        Cap_r_VDD_warrs = Cap_inst_r.get_all_port_pins('VDD')[0]
        Cap_r_VSS_warrs = Cap_inst_r.get_all_port_pins('VSS')[0]
        Cap_lt_VDD_warrs = Cap_inst_l.get_all_port_pins('VDD')[params.cap_params.ny]
        Cap_lb_VDD_warrs = Cap_inst_l.get_all_port_pins('VDD')[params.cap_params.ny + 1]
        Cap_l_VDD_warrs = Cap_inst_l.get_all_port_pins('VDD')[0]
        Cap_l_VSS_warrs = Cap_inst_l.get_all_port_pins('VSS')[0]

        CMFB_VDD_warrs = CMFB_inst.get_all_port_pins('AVDD')[0]
        CMFB_VSS_warrs = CMFB_inst.get_all_port_pins('VSS')[0]

        CMFB_vout_warrs = CMFB_inst.get_all_port_pins('vout')[0]
        CMFB_vbias_warrs = CMFB_inst.get_all_port_pins('vbias')[0]
        CMFB_vip1_warrs = CMFB_inst.get_all_port_pins('vip1')[0]
        CMFB_vip2_warrs = CMFB_inst.get_all_port_pins('vip2')[0]
        CMFB_vin1_warrs = CMFB_inst.get_all_port_pins('vin1')[0]
        CMFB_vin2_warrs = CMFB_inst.get_all_port_pins('vin2')[0]
        CMFB_vsip1_warrs = CMFB_inst.get_all_port_pins('vsip1')[0]
        CMFB_vsip2_warrs = CMFB_inst.get_all_port_pins('vsip2')[0]
        CMFB_vsin1_warrs = CMFB_inst.get_all_port_pins('vsin1')[0]
        CMFB_vsin2_warrs = CMFB_inst.get_all_port_pins('vsin2')[0]
        CMFB_vs1_warrs = CMFB_inst.get_all_port_pins('vs1')[0]
        CMFB_vs2_warrs = CMFB_inst.get_all_port_pins('vs2')[0]

        dRes_r_in1_warrs = dRes_inst_r.get_all_port_pins('in1')[0]
        dRes_r_in2_warrs = dRes_inst_r.get_all_port_pins('in2')[0]
        dRes_r_in3_warrs = dRes_inst_r.get_all_port_pins('in3')[0]
        dRes_r_in4_warrs = dRes_inst_r.get_all_port_pins('in4')[0]
        dRes_r_VDD_warrs = dRes_inst_r.get_all_port_pins('VDD')[0]
        dRes_l_in1_warrs = dRes_inst_l.get_all_port_pins('in1')[0]
        dRes_l_in2_warrs = dRes_inst_l.get_all_port_pins('in2')[0]
        dRes_l_in3_warrs = dRes_inst_l.get_all_port_pins('in3')[0]
        dRes_l_in4_warrs = dRes_inst_l.get_all_port_pins('in4')[0]
        dRes_l_VDD_warrs = dRes_inst_l.get_all_port_pins('VDD')[0]

        HVDD_l_out_warrs = HVDD_inst_l.get_all_port_pins('out')[0]
        HVDD_l_VDD_warrs = HVDD_inst_l.get_all_port_pins('VDD')[1]
        HVDD_l_VSS_warrs = HVDD_inst_l.get_all_port_pins('VSS')[0]
        HVDD_r_out_warrs = HVDD_inst_r.get_all_port_pins('out')[0]
        HVDD_r_VDD_warrs = HVDD_inst_r.get_all_port_pins('VDD')[1]
        HVDD_r_VSS_warrs = HVDD_inst_r.get_all_port_pins('VSS')[0]

        MUX_l_out_warrs = MUX_inst_l.get_all_port_pins('OUT')[1]
        MUX_l_CNT_warrs = MUX_inst_l.get_all_port_pins('CNT')[0]
        MUX_l_B_warrs = MUX_inst_l.get_all_port_pins('B')[0]
        MUX_l_VDD_warrs = MUX_inst_l.get_all_port_pins('VDD')[1]
        MUX_l_VSS_warrs = MUX_inst_l.get_all_port_pins('VSS')[0]

        MUX_r_out_warrs = MUX_inst_r.get_all_port_pins('OUT')[1]
        MUX_r_CNT_warrs = MUX_inst_r.get_all_port_pins('CNT')[0]
        MUX_r_B_warrs = MUX_inst_r.get_all_port_pins('B')[0]
        MUX_r_VDD_warrs = MUX_inst_r.get_all_port_pins('VDD')[1]
        MUX_r_VSS_warrs = MUX_inst_r.get_all_port_pins('VSS')[0]

        PRes_r_Op_warrs = Ps_Res_inst_r.get_all_port_pins('Op')[0]
        PRes_r_On_warrs = Ps_Res_inst_r.get_all_port_pins('On')[0]
        PRes_r_CNT_warrs = Ps_Res_inst_r.get_all_port_pins('CNT')[0]
        PRes_r_IN_warrs = Ps_Res_inst_r.get_all_port_pins('I')[0]
        PRes_r_VDD_warrs = Ps_Res_inst_r.get_all_port_pins('VDD')[1]
        PRes_r_VDD_top_warrs = Ps_Res_inst_r.get_all_port_pins('VDD')[0]
        PRes_r_VSS_warrs = Ps_Res_inst_r.get_all_port_pins('VSS')[0]

        PRes_l_Op_warrs = Ps_Res_inst_l.get_all_port_pins('Op')[0]
        PRes_l_On_warrs = Ps_Res_inst_l.get_all_port_pins('On')[0]
        PRes_l_CNT_warrs = Ps_Res_inst_l.get_all_port_pins('CNT')[0]
        PRes_l_IN_warrs = Ps_Res_inst_l.get_all_port_pins('I')[0]
        PRes_l_VDD_warrs = Ps_Res_inst_l.get_all_port_pins('VDD')[1]
        PRes_l_VDD_top_warrs = Ps_Res_inst_l.get_all_port_pins('VDD')[0]
        PRes_l_VSS_warrs = Ps_Res_inst_l.get_all_port_pins('VSS')[0]

        MUX_SW_l_out_warrs = MUX_SW_inst_l.get_all_port_pins('OUT')[0]
        MUX_SW_l_CNT_warrs = MUX_SW_inst_l.get_all_port_pins('CNT')[0]
        MUX_SW_l_A_warrs = MUX_SW_inst_l.get_all_port_pins('A')[0]
        MUX_SW_l_VDD_warrs = MUX_SW_inst_l.get_all_port_pins('VDD')[0]
        MUX_SW_l_VSS_warrs = MUX_SW_inst_l.get_all_port_pins('VSS')[0]

        MUX_SW_r_out_warrs = MUX_SW_inst_r.get_all_port_pins('OUT')[0]
        MUX_SW_r_CNT_warrs = MUX_SW_inst_r.get_all_port_pins('CNT')[0]
        MUX_SW_r_A_warrs = MUX_SW_inst_r.get_all_port_pins('A')[0]
        MUX_SW_r_B_warrs = MUX_SW_inst_r.get_all_port_pins('B')[0]
        MUX_SW_r_VDD_warrs = MUX_SW_inst_r.get_all_port_pins('VDD')[0]
        MUX_SW_r_VSS_warrs = MUX_SW_inst_r.get_all_port_pins('VSS')[0]

        # get vertical TrackIDs
        top_w = self.grid.get_track_width(top_layer, 1, unit_mode=True)
        vm_w = self.grid.get_min_track_width(vm_layer, top_w=top_w, unit_mode=True)  # Set Track width
        bot_w = self.grid.get_min_track_width(bot_layer, unit_mode=True)

        # Connect "Amp_outn" to next metal, adjust the coordinate of tracks
        lay_id = Amp_outn_warr.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, Amp_outn_warr.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(Amp_outn_warr.layer_id + 1, Amp_outn_warr.upper + 0.5, half_track=True)
        Amp_outn_warr1 = self.connect_to_tracks(Amp_outn_warr, TrackID(lay_id, mid_tr),
                                                track_lower=mid_coord - min_len / 2,
                                                track_upper=mid_coord + min_len / 2)

        # Connect "Amp_xp" to next metal, adjust the coordinate of tracks
        lay_id = Amp_xp_warrs.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, Amp_xp_warrs.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(Amp_xp_warrs.layer_id + 1, col_list[0] + col_list[1] - (2 * x_space),
                                           unit_mode=True) + 1
        '''Amp_xp_warrs1 = self.connect_to_tracks(Amp_xp_warrs, TrackID(lay_id, mid_tr),
                                               track_lower=mid_coord - min_len / 2,
                                               track_upper=mid_coord + min_len / 2)'''

        # Connect "Amp_outp" to next metal, adjust the coordinate of tracks
        lay_id = Amp_outp_warr.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, Amp_outp_warr.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(Amp_outp_warr.layer_id + 1, Amp_outp_warr.lower - 0.5, half_track=True)
        Amp_outp_warr1 = self.connect_to_tracks(Amp_outp_warr, TrackID(lay_id, mid_tr),
                                                track_lower=mid_coord - min_len / 2,
                                                track_upper=mid_coord + min_len / 2)

        # Connect "Amp_xn" to next metal, adjust the coordinate of tracks
        lay_id = Amp_xn_warrs.layer_id
        mid_coord = self.grid.track_to_coord(lay_id, Amp_xn_warrs.track_id.base_index)
        lay_id += 1
        min_len = self.grid.get_min_length(lay_id, 1)
        mid_tr = self.grid.find_next_track(Amp_xn_warrs.layer_id + 1, col_list[0] - (col_list[1] - (2 * x_space)),
                                           unit_mode=True) - 1
        '''Amp_xn_warrs1 = self.connect_to_tracks(Amp_xn_warrs, TrackID(lay_id, mid_tr),
                                               track_lower=mid_coord - min_len / 2,
                                               track_upper=mid_coord + min_len / 2)'''

        Cap_r_plus_tid = TrackID(
            top_layer,
            self.grid.coord_to_nearest_track(top_layer,
                                             self.grid.track_to_coord(hm_layer,
                                                                      Cap_r_plus_warr.track_id.base_index)) + 4,
            width=vm_w
        )
        CMFB_vout_m_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, CMFB_vout_warrs.middle),
                                  width=vm_w)
        CMFB_vin1_u_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, CMFB_vin1_warrs.upper),
                                  width=vm_w)

        Amp_outn_u_tid = TrackID(top_layer, self.grid.coord_to_nearest_track(top_layer, Amp_outn_warr1.upper),
                                 width=vm_w)
        Amp_outn_au_tid = TrackID(top_layer, self.grid.coord_to_nearest_track(top_layer, Amp_outn_warr1.upper) + 1,
                                  width=vm_w)  # one track after the track assigned to "Amp_outn_warr1.upper"

        tid_MUX_SW_l_A = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, MUX_SW_l_A_warrs.upper) + vm_w,
                                 width=vm_w)
        tid_MUX_SW_r_A = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, MUX_SW_r_VSS_warrs.upper),
                                 width=vm_w)
        tid_Amp_cmfb_ibias = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Amp_cmfb_ibias_warrs.lower),
                                     width=vm_w)
        ## Setting tracks for VSS routing
        tid_VSS_l_top = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, 
                                col_list[0] + (col_list[1] - col_list[0])//2, unit_mode=True) ,   ##Used for VSS connections to top-left blocks
                                width=vm_w)
        tid_VSS_mid = TrackID(hm_layer, self.grid.coord_to_nearest_track(hm_layer, 
                                row_list[1] - (row_list[1] - row_list[0])//3, unit_mode=True) ,   ##Used for shorting top and bottom block's VSS connection
                                width=vm_w)
        
        tid_VSS_r_top = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True) - 10*vm_w,
                               width=vm_w)

        # Set track-ids for vertical line in empty space between columns, using metal 3 on left and right sides
        tid_x_col_r1_M3 = TrackID(bot_layer,
                                  self.grid.coord_to_nearest_track(bot_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                   unit_mode=True),
                                  width=bot_w)
        tid_x_col_l1_M3 = TrackID(bot_layer, self.grid.coord_to_nearest_track(bot_layer, col_list[0] - (
                    col_list[1] - (2 * x_space)), unit_mode=True),
                                  width=bot_w)
        tid_x_col_r2_M3 = TrackID(bot_layer,
                                  self.grid.coord_to_nearest_track(bot_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                   unit_mode=True) - bot_w,
                                  width=bot_w)
        tid_x_col_l2_M3 = TrackID(bot_layer, self.grid.coord_to_nearest_track(bot_layer, col_list[0] - (
                    col_list[1] - (2 * x_space)), unit_mode=True) + bot_w,
                                  width=bot_w)
        tid_x_col_r3_M3 = TrackID(bot_layer,
                                  self.grid.coord_to_nearest_track(bot_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                   unit_mode=True) - 2*bot_w,
                                  width=bot_w)
        tid_x_col_l3_M3 = TrackID(bot_layer, self.grid.coord_to_nearest_track(bot_layer, col_list[0] - (
                    col_list[1] - (2 * x_space)), unit_mode=True) + 2*bot_w,
                                  width=bot_w)

        # Set track-ids for vertical line in empty space between columns, using metal 5 on right side
        tid_x_col_r1 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True) - 5*vm_w,
                               width=vm_w)
        tid_x_col_r3 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True) - 4*vm_w,
                               width=vm_w)
        tid_x_col_r4 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True) - 3*vm_w,
                               width=vm_w)
        tid_x_col_r5 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True) - 2*vm_w,
                               width=vm_w)
        tid_x_col_r6 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True),
                               width=vm_w)
        tid_x_col_r7 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] + col_list[1] - (2 * x_space),
                                                                unit_mode=True) + vm_w,
                               width=vm_w)

        # Set track-ids for vertical line in empty space between columns, using metal 5 on left side
        tid_x_col_l1 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True) + 5*vm_w,
                               width=vm_w)
        tid_x_col_l3 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True) + 4*vm_w,
                               width=vm_w)
        tid_x_col_l4 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True) + 3*vm_w,
                               width=vm_w)
        tid_x_col_l5 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True) + 2*vm_w,
                               width=vm_w)
        tid_x_col_l6 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True) + vm_w,
                               width=vm_w)
        tid_x_col_l7 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True),
                               width=vm_w)
        tid_x_col_l8 = TrackID(vm_layer,
                               self.grid.coord_to_nearest_track(vm_layer, col_list[0] - (col_list[1] - (2 * x_space)),
                                                                unit_mode=True) - vm_w,
                               width=vm_w)

        # Set track-ids for Horizontal line in empty space between rows, using metal 4
        tid_y_row_20 = TrackID(hm_layer, self.grid.coord_to_nearest_track(hm_layer, (row_list[2] - y_space),
                                                                          unit_mode=True),
                               width=vm_w)
        tid_y_row_21 = TrackID(hm_layer, self.grid.coord_to_nearest_track(hm_layer, (row_list[2] - y_space),
                                                                          unit_mode=True) + vm_w,
                               width=vm_w)

        # Connect Differential resistor on right side using M1 and M2(This part is already taken care by the resistor_diff generator....
        # we can directly use the layer 2 ports of resistor_diff generator to access its terminals)
        '''Res_P2_tid = TrackID(2, dRes_r_in2_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_M2_tid = TrackID(2, dRes_r_in1_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_P1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_r_in2_warrs.lower) + 2,
                             width=2 * vm_w)
        Res_M1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_r_in1_warrs.lower) + 2,
                             width=2 * vm_w)

        RP1 = self.add_wires(1, Res_P1_tid.base_index,
                             dRes_r_in2_warrs.get_bbox_array(grid).bottom_unit - pitch_vm // 2,
                             dRes_r_in2_warrs.get_bbox_array(grid).bottom_unit, unit_mode=True, width=2)
        dRes_r_in2_warrs1 = self.connect_to_tracks([RP1], Res_P2_tid)

        RM1 = self.add_wires(1, Res_M1_tid.base_index, dRes_r_in1_warrs.get_bbox_array(grid).top_unit - pitch_vm // 2,
                             dRes_r_in1_warrs.get_bbox_array(grid).top_unit, unit_mode=True, width=2)
        dRes_r_in1_warrs1 = self.connect_to_tracks([RM1], Res_M2_tid)

        Res_P2_tid = TrackID(2, dRes_r_in3_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_M2_tid = TrackID(2, dRes_r_in4_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_P1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_r_in3_warrs.lower) + 2,
                             width=2 * vm_w)
        Res_M1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_r_in4_warrs.lower) + 2,
                             width=2 * vm_w)

        RP1 = self.add_wires(1, Res_P1_tid.base_index,
                             dRes_r_in3_warrs.get_bbox_array(grid).bottom_unit - pitch_vm // 2,
                             dRes_r_in3_warrs.get_bbox_array(grid).bottom_unit, unit_mode=True, width=2)
        dRes_r_in3_warrs1 = self.connect_to_tracks([RP1], Res_P2_tid)

        RM1 = self.add_wires(1, Res_M1_tid.base_index, dRes_r_in4_warrs.get_bbox_array(grid).top_unit - pitch_vm // 2,
                             dRes_r_in4_warrs.get_bbox_array(grid).top_unit, unit_mode=True, width=2)
        dRes_r_in4_warrs1 = self.connect_to_tracks([RM1], Res_M2_tid)

        # Connect Differential resistor on left side
        Res_P2_tid = TrackID(2, dRes_l_in2_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_M2_tid = TrackID(2, dRes_l_in1_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_P1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_l_in2_warrs.lower) + 2,
                             width=2 * vm_w)
        Res_M1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_l_in1_warrs.lower) + 2,
                             width=2 * vm_w)

        RP1 = self.add_wires(1, Res_P1_tid.base_index,
                             dRes_l_in2_warrs.get_bbox_array(grid).bottom_unit - pitch_vm // 2,
                             dRes_l_in2_warrs.get_bbox_array(grid).bottom_unit, unit_mode=True, width=2)
        dRes_l_in2_warrs1 = self.connect_to_tracks([RP1], Res_P2_tid)

        RM1 = self.add_wires(1, Res_M1_tid.base_index, dRes_l_in1_warrs.get_bbox_array(grid).top_unit - pitch_vm // 2,
                             dRes_l_in1_warrs.get_bbox_array(grid).top_unit, unit_mode=True, width=2)
        dRes_l_in1_warrs1 = self.connect_to_tracks([RM1], Res_M2_tid)

        Res_P2_tid = TrackID(2, dRes_l_in3_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_M2_tid = TrackID(2, dRes_l_in4_warrs.track_id.base_index - 2,
                             width=vm_w)
        Res_P1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_l_in3_warrs.lower) + 2,
                             width=2 * vm_w)
        Res_M1_tid = TrackID(1, self.grid.coord_to_nearest_track(1, dRes_l_in4_warrs.lower) + 2,
                             width=2 * vm_w)

        RP1 = self.add_wires(1, Res_P1_tid.base_index,
                             dRes_l_in3_warrs.get_bbox_array(grid).bottom_unit - pitch_vm // 2,
                             dRes_l_in3_warrs.get_bbox_array(grid).bottom_unit, unit_mode=True, width=2)
        dRes_l_in3_warrs1 = self.connect_to_tracks([RP1], Res_P2_tid)

        RM1 = self.add_wires(1, Res_M1_tid.base_index, dRes_l_in4_warrs.get_bbox_array(grid).top_unit - pitch_vm // 2,
                             dRes_l_in4_warrs.get_bbox_array(grid).top_unit, unit_mode=True, width=2)
        dRes_l_in4_warrs1 = self.connect_to_tracks([RM1], Res_M2_tid)'''

        grid_helper = RoutingGridHelper(template_base=self,
                                        unit_mode=True,
                                        layer_id=vm_layer,  # not used here
                                        track_width=1)  # not used here

        # Connect Tracks using defined Track_ids
        CMFB_vout_Amp = self.connect_to_tracks([CMFB_vout_warrs, Amp_cmbias_warrs], CMFB_vout_m_tid)

        #Cap_r_minus_warr = self.connect_to_tracks([Cap_r_minus_warrs], tid_x_col_r6)
        
        #Amp_outn_Cap = self.connect_to_tracks([Cap_r_minus_warr, Amp_outn_warr1], Amp_outn_u_tid)
        Amp_outn_Cap = self.connect_to_tracks([Cap_r_minus_warrs,Amp_outn_warr], tid_x_col_r6)

        '''Amp_xp_Cap = self.connect_to_tracks(
            [
                grid_helper.connect_level_up(warr_id=Cap_r_plus_warr,
                                             via_dir=ViaDirection.TOP,
                                             step=0.0),
                Amp_xp_warrs1
            ],
            Cap_r_plus_tid)'''
        Amp_xp_Cap = self.connect_to_tracks(
            [
                Cap_r_plus_warr,
                Amp_xp_warrs
            ],
            tid_x_col_r6)
        

        #Cap_l_minus_warr = self.connect_to_tracks([Cap_l_minus_warrs], tid_x_col_l6)
        #Amp_outp_Cap = self.connect_to_tracks([Cap_l_minus_warr, Amp_outp_warr1], Amp_outn_au_tid)
        Amp_outp_Cap = self.connect_to_tracks([Cap_l_minus_warrs, Amp_outp_warr], tid_x_col_l6)
        
        '''Amp_xn_Cap = self.connect_to_tracks(
            [
                grid_helper.connect_level_up(warr_id=Cap_l_plus_warr,
                                             via_dir=ViaDirection.TOP,
                                             step=0.0),
                Amp_xn_warrs1
            ],
            Cap_r_plus_tid)'''
        Amp_xn_Cap = self.connect_to_tracks(
            [
                Cap_l_plus_warr,
                Amp_xn_warrs
            ],
            tid_x_col_l6)
       
        Amp_outp_CMFB = self.connect_to_tracks([CMFB_vip2_warrs, Amp_outp_warr], tid_x_col_r1)
        Amp_outn_CMFB = self.connect_to_tracks([CMFB_vip1_warrs, Amp_outn_warr], tid_x_col_l1)

        '''CMFB_vs1_dRes = self.connect_to_tracks([dRes_r_in1_warrs, dRes_r_in3_warrs, CMFB_vs1_warrs],
                                               tid_x_col_r1_M3)
        CMFB_vs2_dRes = self.connect_to_tracks([dRes_l_in1_warrs, dRes_l_in3_warrs, CMFB_vs2_warrs],
                                               tid_x_col_l1_M3)'''
        CMFB_vs1_dRes = self.connect_to_tracks([dRes_r_in1_warrs, dRes_r_in3_warrs, CMFB_vs1_warrs],
                                               tid_x_col_r1_M3)
        CMFB_vs2_dRes = self.connect_to_tracks([dRes_l_in1_warrs, dRes_l_in3_warrs, CMFB_vs2_warrs],
                                               tid_x_col_l1_M3)

        CMFB_vsin1_dRes = self.connect_to_tracks([dRes_r_in2_warrs, CMFB_vsin1_warrs], tid_x_col_r2_M3)
        CMFB_vsip1_dRes = self.connect_to_tracks([dRes_r_in4_warrs, CMFB_vsip1_warrs], tid_x_col_r3_M3)

        CMFB_vsin2_dRes = self.connect_to_tracks([dRes_l_in2_warrs, CMFB_vsin2_warrs], tid_x_col_l2_M3)
        CMFB_vsip2_dRes = self.connect_to_tracks([dRes_l_in4_warrs, CMFB_vsip2_warrs], tid_x_col_l3_M3)
        
        CMFB_vin1_vin2 = self.connect_to_tracks([CMFB_vin2_warrs, CMFB_vin1_warrs], CMFB_vin1_u_tid)
        CMFB_vin1_MUX = self.connect_to_tracks([CMFB_vin1_warrs, MUX_r_out_warrs], tid_x_col_r3)

        Amp_ref_in_MUX = self.connect_to_tracks([MUX_l_out_warrs, Amp_ref_in_warrs], tid_x_col_l4)
        Amp_ref_in_PRes = self.connect_to_tracks([PRes_l_IN_warrs, Amp_ref_in_warrs], tid_x_col_l4)

        MUX_SW_r_VSS_warr = self.connect_to_tracks([MUX_SW_r_VSS_warrs, MUX_SW_r_A_warrs, MUX_SW_r_B_warrs],
                                                   tid_MUX_SW_r_A)
        MUX_SW_r_VSS = self.connect_to_tracks([MUX_SW_r_out_warrs, MUX_SW_r_VSS_warr], tid_y_row_21)

        MUX_r_B_HVDD = self.connect_to_tracks([HVDD_r_out_warrs, MUX_r_B_warrs], tid_x_col_r6)
        MUX_l_B_HVDD = self.connect_to_tracks([HVDD_l_out_warrs, MUX_l_B_warrs], tid_x_col_l6)
        Amp_inp_PRes = self.connect_to_tracks([PRes_l_Op_warrs, Amp_inp_warrs], tid_x_col_l6)
        Amp_inn_PRes = self.connect_to_tracks([PRes_l_On_warrs, Amp_inn_warrs], tid_x_col_l7)
        CMFB_vbias_MUX_SW = self.connect_to_tracks([MUX_SW_l_out_warrs, CMFB_vbias_warrs], tid_y_row_21)
        vtid_MUX_SW_l_A = self.connect_to_tracks([MUX_SW_l_A_warrs], tid_MUX_SW_l_A)
        vtid_Amp_cmfb_ibias = self.connect_to_tracks([Amp_cmfb_ibias_warrs], tid_Amp_cmfb_ibias)
        Amp_cmfb_ibias_MUX_SW = self.connect_to_tracks([vtid_Amp_cmfb_ibias, vtid_MUX_SW_l_A], tid_y_row_20)

        # Connect all VDD tracks left side
        dRes_l_VDD_warrs1 = grid_helper.connect_level_up(grid_helper.connect_level_up(dRes_l_VDD_warrs,ViaDirection.BOTTOM, step=0), ViaDirection.BOTTOM, step=0)
        VDD_l = self.connect_to_tracks(
            [Amp_VDD_warrs, CMFB_VDD_warrs, Cap_lt_VDD_warrs, Cap_lb_VDD_warrs, Cap_l_VDD_warrs, MUX_l_VDD_warrs,
             MUX_SW_l_VDD_warrs, HVDD_l_VDD_warrs, PRes_l_VDD_warrs, dRes_l_VDD_warrs1], tid_x_col_l5)

        # Connect all VSS tracks left side
        '''VSS_l = self.connect_to_tracks(
            [Amp_VSS_warrs, CMFB_VSS_warrs, MUX_l_VSS_warrs, MUX_SW_l_VSS_warrs, HVDD_l_VSS_warrs, PRes_l_VSS_warrs,
             Cap_l_VSS_warrs], tid_x_col_l4)'''
        VSS_l_bot = self.connect_to_tracks(
          [MUX_SW_l_VSS_warrs,MUX_l_VSS_warrs,HVDD_l_VSS_warrs,PRes_l_VSS_warrs], tid_x_col_l8) ## Connecting VSS of blocks in bottom-left
        VSS_l_top = self.connect_to_tracks(
          [Cap_l_VSS_warrs,CMFB_VSS_warrs,Amp_VSS_warrs], tid_VSS_l_top) ## Connecting VSS of blocks in top-left
        VSS_l = self.connect_to_tracks(
            [VSS_l_bot, VSS_l_top], tid_VSS_mid)

        # Connect all VDD tracks right side
        dRes_r_VDD_warrs1 = grid_helper.connect_level_up(grid_helper.connect_level_up(dRes_r_VDD_warrs,ViaDirection.BOTTOM, step=0), ViaDirection.BOTTOM, step=0)
        VDD_r = self.connect_to_tracks(
            [Amp_VDD_warrs, CMFB_VDD_warrs, Cap_rt_VDD_warrs, Cap_rb_VDD_warrs, Cap_r_VDD_warrs, MUX_r_VDD_warrs,
             HVDD_r_VDD_warrs, MUX_SW_r_VDD_warrs, PRes_r_VDD_warrs, PRes_r_On_warrs, PRes_r_Op_warrs, PRes_r_IN_warrs,
             PRes_r_CNT_warrs, dRes_r_VDD_warrs1], tid_x_col_r5)

        # Connect all VSS tracks right side
        '''VSS_r = self.connect_to_tracks(
            [Amp_VSS_warrs, CMFB_VSS_warrs, MUX_r_VSS_warrs, MUX_SW_r_VSS_warrs, HVDD_r_VSS_warrs,
             PRes_r_VSS_warrs, MUX_SW_r_CNT_warrs, Cap_r_VSS_warrs], tid_x_col_r4)'''
        VSS_r_bot = self.connect_to_tracks(
          [MUX_SW_r_VSS_warrs,MUX_r_VSS_warrs,HVDD_r_VSS_warrs,PRes_r_VSS_warrs,MUX_SW_r_CNT_warrs], tid_x_col_r7) ## Connecting VSS terminals of blocks in bottom-right
        VSS_r_top = self.connect_to_tracks(
          [Amp_VSS_warrs, CMFB_VSS_warrs, Cap_r_VSS_warrs], tid_VSS_r_top) ## Connecting VSS terminals of blocks in bottom-right
        VSS_r = self.connect_to_tracks(
            [VSS_r_bot, VSS_r_top], tid_VSS_mid)

        # Set Bounding Box and size of top blocks
        tot_box = Amp_inst.bound_box.merge(CMFB_inst.bound_box)
        for inst in [dRes_inst_l, dRes_inst_r, MUX_inst_r, MUX_inst_l, MUX_SW_inst_l, MUX_SW_inst_r, Cap_inst_r,
                     Cap_inst_l, HVDD_inst_l, HVDD_inst_r, Ps_Res_inst_r, Ps_Res_inst_l]:
            tot_box = tot_box.merge(inst.bound_box)
        self.set_size_from_bound_box(top_layer - 1, tot_box, round_up=True)

        # Add supply grids, M2 for VDD and M1 for VSS

        vdd_layer_name = grid.tech_info.get_layer_name(2)
        vss_layer_name = grid.tech_info.get_layer_name(1)

        res = grid.resolution
        for row in {row_list[0], row_list[1], row_list[2], row_list[3]}:
            box = BBox(tot_box.left_unit - 10 * pitch_vm, row - 8 * pitch_vm,
                       tot_box.right_unit + 10 * pitch_vm, row - 4 * pitch_vm,
                       res, unit_mode=True)
            if row != row_list[0]:
                self.add_rect(vdd_layer_name, box)
            if row == row_list[3]:
                grid_helper.connect_warr_to_box(Amp_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(Cap_rb_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(Cap_lb_VDD_warrs, box, pitch_vm, vdd_layer_name)
            if row == row_list[2]:
                grid_helper.connect_warr_to_box(CMFB_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(Cap_rt_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(Cap_lt_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(MUX_SW_r_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(MUX_SW_l_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(PRes_l_VDD_top_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(PRes_r_VDD_top_warrs, box, pitch_vm, vdd_layer_name)
                
                box2 = Cap_lt_VDD_warrs.get_bbox_array(grid)
                # self.add_rect(vdd_layer_name, BBox(box2.left_unit, box2.bottom_unit, box2.right_unit,
                #                          box2.top_unit + pitch_vm // 2, res, unit_mode=True), unit_mode=True)
                box3 = dRes_l_VDD_warrs.get_bbox_array(grid)
                self.add_rect(vdd_layer_name, BBox(box3.right_unit - pitch_vm // 2, box2.bottom_unit, box3.right_unit,
                                         box3.bottom_unit, res, unit_mode=True), unit_mode=True)
                box2 = Cap_rt_VDD_warrs.get_bbox_array(grid)
                # self.add_rect(vdd_layer_name, BBox(box2.left_unit, box2.bottom_unit, box2.right_unit,
                #                          box2.top_unit + pitch_vm // 2, res, unit_mode=True), unit_mode=True)
                box3 = dRes_r_VDD_warrs.get_bbox_array(grid)
                self.add_rect(vdd_layer_name, BBox(box3.left_unit, box2.bottom_unit, box3.left_unit + pitch_vm // 2,
                                         box3.bottom_unit, res, unit_mode=True), unit_mode=True)
            if row == row_list[1]:
                grid_helper.connect_warr_to_box(MUX_r_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(MUX_l_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(HVDD_l_VDD_warrs, box, pitch_vm, vdd_layer_name)
                grid_helper.connect_warr_to_box(HVDD_r_VDD_warrs, box, pitch_vm, vdd_layer_name)

            # VSS lines
            box = BBox(tot_box.left_unit - 10 * pitch_vm, row - 8 * pitch_vm,
                       tot_box.right_unit + 10 * pitch_vm, row - 4 * pitch_vm,
                       res, unit_mode=True)
            self.add_rect(vss_layer_name, box)
            if row == row_list[2]:
                grid_helper.connect_warr_to_box(Amp_VSS_warrs, box, pitch_vm, vss_layer_name)
            if row == row_list[1]:
                grid_helper.connect_warr_to_box(CMFB_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(MUX_SW_r_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(MUX_SW_l_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(PRes_l_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(PRes_r_VSS_warrs, box, pitch_vm, vss_layer_name)
            if row == row_list[0]:
                grid_helper.connect_warr_to_box(MUX_r_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(MUX_l_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(HVDD_l_VSS_warrs, box, pitch_vm, vss_layer_name)
                grid_helper.connect_warr_to_box(HVDD_r_VSS_warrs, box, pitch_vm, vss_layer_name)

            box = BBox(col_list[0] - col_list[1] + 4 * pitch_vm, tot_box.bottom_unit - 10 * pitch_vm,
                       col_list[0] - col_list[1] + 6 * pitch_vm, tot_box.top_unit + 10 * pitch_vm,
                       res, unit_mode=True)
            self.add_rect(vss_layer_name, box)
            box = BBox(col_list[0] + col_list[1] - 6 * pitch_vm, tot_box.bottom_unit - 10 * pitch_vm,
                       col_list[0] + col_list[1] - 4 * pitch_vm, tot_box.top_unit + 10 * pitch_vm,
                       res, unit_mode=True)
            self.add_rect(vss_layer_name, box)
            self.add_pin('VSS',[MUX_r_VSS_warrs,MUX_l_VSS_warrs],show=True)

        # re-export pins on subcells.
        self.reexport(Amp_inst.get_port('inp'), show=params.show_pins)
        self.reexport(Amp_inst.get_port('VDD'), show=params.show_pins)
        self.reexport(Amp_inst.get_port('VSS'), show=params.show_pins)
        self.reexport(Amp_inst.get_port('inn'), show=params.show_pins)
        self.reexport(Amp_inst.get_port('outp'), net_name='outp', show=params.show_pins)
        self.reexport(Amp_inst.get_port('outn'), net_name='outn', show=params.show_pins)
        self.reexport(Amp_inst.get_port('bias'), net_name='ibias', show=params.show_pins)
        self.reexport(MUX_inst_r.get_port('A'), net_name='voutcm', show=params.show_pins)
        self.reexport(MUX_inst_r.get_port('CNT'), net_name='CNT0', show=params.show_pins)
        self.reexport(MUX_inst_l.get_port('A'), net_name='ref', show=params.show_pins)
        self.reexport(MUX_inst_l.get_port('CNT'), net_name='CNT1', show=params.show_pins)
        self.reexport(MUX_SW_inst_l.get_port('CNT'), net_name='CNT2', show=params.show_pins)
        self.reexport(MUX_SW_inst_l.get_port('B'), net_name='cmfb_bias', show=params.show_pins)
        self.reexport(Ps_Res_inst_l.get_port('CNT'), net_name='CNT3', show=params.show_pins)
        self.reexport(Amp_inst.get_port('en_amp'), net_name='en_Amp', show=params.show_pins)

        self.reexport(CMFB_inst.get_port('en'), net_name='en_CMFB', show=params.show_pins)
        self.reexport(CMFB_inst.get_port('AVDD'), net_name='VDD', show=params.show_pins)
        self.reexport(CMFB_inst.get_port('VSS'), show=params.show_pins)
        self.reexport(Cap_inst_l.get_port('VDD'), net_name='VDD', show=params.show_pins)
        self.reexport(Cap_inst_r.get_port('VDD'), net_name='VDD', show=params.show_pins)

        # compute schematic parameters.
        self._sch_params = dict(
            opamp_sch_params=opamp_master.sch_params,
            cmfb_sch_params=cmfb_master.sch_params,
            cap_sch_params=cap_master.sch_params,
            mux_2to1_sch_params=mux_2to1_master.sch_params,
            mux_sw_sch_params=mux_sw_master.sch_params,
            hvdd_sch_params=hvdd_master.sch_params,
            dres_sch_params=dres_master.sch_params,
            pres_sch_params=pres_master.sch_params,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class opamp_two_stage_top(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
