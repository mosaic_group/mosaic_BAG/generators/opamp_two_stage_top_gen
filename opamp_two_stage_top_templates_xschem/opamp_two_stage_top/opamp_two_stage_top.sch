v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 1490 -660 1580 -660 {
lab=hvdd_out2}
N 1490 -890 1580 -890 {
lab=hvdd_out1}
C {devices/iopin.sym} 140 -1280 0 0 {name=p1 lab=inp}
C {devices/iopin.sym} 140 -1260 0 0 {name=p2 lab=inn}
C {devices/ipin.sym} 140 -1240 0 0 {name=p3 lab=voutcm}
C {devices/ipin.sym} 140 -1220 0 0 {name=p4 lab=ibias}
C {devices/iopin.sym} 140 -1350 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 140 -1330 2 0 {name=p6 lab=VSS}
C {two_stage_opamp_nmos_gen/two_stage_opamp_nmos_templates_xschem/two_stage_opamp_nmos/two_stage_opamp_nmos.sym} 350 -1150 0 0 {name=AMP_Core
spiceprefix=X
}
C {devices/iopin.sym} 140 -1180 2 0 {name=p7 lab=cmfb_bias}
C {devices/iopin.sym} 140 -1160 2 0 {name=p8 lab=en_Amp}
C {devices/iopin.sym} 140 -1140 2 0 {name=p9 lab=en_CMFB}
C {devices/opin.sym} 120 -1110 0 0 {name=p10 lab=outp}
C {devices/opin.sym} 120 -1070 0 0 {name=p11 lab=CNT0}
C {devices/opin.sym} 120 -1050 0 0 {name=p12 lab=CNT1}
C {devices/opin.sym} 120 -1030 0 0 {name=p13 lab=CNT2}
C {devices/opin.sym} 120 -1010 0 0 {name=p14 lab=CNT3}
C {devices/opin.sym} 120 -1090 0 0 {name=p15 lab=outn}
C {devices/lab_pin.sym} 470 -1340 1 0 {name=l19 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 470 -1160 3 0 {name=l20 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 490 -1160 3 0 {name=l21 sig_type=std_logic lab=cmfb_ibias}
C {devices/lab_pin.sym} 590 -1300 2 0 {name=l22 sig_type=std_logic lab=xp}
C {devices/lab_pin.sym} 590 -1280 2 0 {name=l23 sig_type=std_logic lab=xn}
C {devices/lab_pin.sym} 590 -1260 2 0 {name=l24 sig_type=std_logic lab=outp}
C {devices/lab_pin.sym} 590 -1240 2 0 {name=l25 sig_type=std_logic lab=outn}
C {devices/lab_pin.sym} 590 -1220 2 0 {name=l26 sig_type=std_logic lab=midp}
C {devices/lab_pin.sym} 590 -1200 2 0 {name=l27 sig_type=std_logic lab=midn}
C {devices/lab_pin.sym} 360 -1320 0 0 {name=l28 sig_type=std_logic lab=ref_in}
C {devices/lab_pin.sym} 360 -1290 0 0 {name=l29 sig_type=std_logic lab=en_Amp}
C {devices/lab_pin.sym} 360 -1260 0 0 {name=l30 sig_type=std_logic lab=inp}
C {devices/lab_pin.sym} 360 -1240 0 0 {name=l31 sig_type=std_logic lab=inn}
C {devices/lab_pin.sym} 360 -1210 0 0 {name=l32 sig_type=std_logic lab=out_loop}
C {devices/lab_pin.sym} 360 -1180 0 0 {name=l33 sig_type=std_logic lab=ibias}
C {cap_mom_array_gen/cap_mom_array_templates_xschem/cap_mom_array/cap_mom_array.sym} 810 -1200 0 0 {name=CCP
spiceprefix=X
}
C {devices/lab_pin.sym} 870 -1310 1 0 {name=l18 sig_type=std_logic lab=outp}
C {devices/lab_pin.sym} 870 -1210 3 0 {name=l34 sig_type=std_logic lab=xn}
C {devices/lab_pin.sym} 810 -1260 0 0 {name=l35 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 930 -1260 2 0 {name=l36 sig_type=std_logic lab=VSS}
C {cap_mom_array_gen/cap_mom_array_templates_xschem/cap_mom_array/cap_mom_array.sym} 1100 -1200 0 0 {name=CCN
spiceprefix=X
}
C {devices/lab_pin.sym} 1160 -1310 1 0 {name=l37 sig_type=std_logic lab=outn}
C {devices/lab_pin.sym} 1160 -1210 3 0 {name=l38 sig_type=std_logic lab=xp}
C {devices/lab_pin.sym} 1100 -1260 0 0 {name=l39 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1220 -1260 2 0 {name=l40 sig_type=std_logic lab=VSS}
C {mux_sw_gen/mux_sw_templates_xschem/mux_sw/mux_sw.sym} 800 -830 0 0 {name=MUX2
spiceprefix=X
}
C {devices/lab_pin.sym} 860 -950 1 0 {name=l41 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 860 -840 3 0 {name=l42 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 890 -950 1 0 {name=l43 sig_type=std_logic lab=CNT2}
C {devices/lab_pin.sym} 950 -900 2 0 {name=l44 sig_type=std_logic lab=cmfb_i}
C {devices/lab_pin.sym} 830 -910 0 0 {name=l45 sig_type=std_logic lab=cmfb_ibias}
C {devices/lab_pin.sym} 830 -880 0 0 {name=l46 sig_type=std_logic lab=cmfb_bias}
C {mux_sw_gen/mux_sw_templates_xschem/mux_sw/mux_sw.sym} 800 -580 0 0 {name=DUM_MUX
spiceprefix=X
}
C {devices/lab_pin.sym} 860 -700 1 0 {name=l47 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 860 -590 3 0 {name=l48 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 890 -700 1 0 {name=l49 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 950 -650 2 0 {name=l50 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 830 -660 0 0 {name=l51 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 830 -630 0 0 {name=l52 sig_type=std_logic lab=VSS}
C {half_vdd_gen/half_vdd_templates_xschem/half_vdd/half_vdd.sym} 1340 -830 0 0 {name=HVDD1
spiceprefix=X
}
C {mux_2to1_gen/mux_2to1_templates_xschem/mux_2to1/mux_2to1.sym} 1550 -840 0 0 {name=MUX1
spiceprefix=X
}
C {devices/lab_pin.sym} 1610 -970 1 0 {name=l53 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1610 -850 3 0 {name=l54 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1400 -850 3 0 {name=l55 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1400 -950 1 0 {name=l56 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1700 -910 2 0 {name=l57 sig_type=std_logic lab=ref_in}
C {devices/lab_pin.sym} 1580 -920 0 0 {name=l58 sig_type=std_logic lab=ref}
C {devices/lab_pin.sym} 1640 -970 1 0 {name=l59 sig_type=std_logic lab=CNT1}
C {half_vdd_gen/half_vdd_templates_xschem/half_vdd/half_vdd.sym} 1340 -600 0 0 {name=HVDD0
spiceprefix=X
}
C {mux_2to1_gen/mux_2to1_templates_xschem/mux_2to1/mux_2to1.sym} 1550 -610 0 0 {name=MUX0
spiceprefix=X
}
C {devices/lab_pin.sym} 1610 -740 1 0 {name=l60 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1610 -620 3 0 {name=l61 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1400 -620 3 0 {name=l62 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1400 -720 1 0 {name=l63 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1700 -680 2 0 {name=l64 sig_type=std_logic lab=vcm}
C {devices/lab_pin.sym} 1580 -690 0 0 {name=l65 sig_type=std_logic lab=voutcm}
C {devices/lab_pin.sym} 1640 -740 1 0 {name=l66 sig_type=std_logic lab=CNT0}
C {pseudo_resistor_ctrl_gen/pseudo_resistor_ctrl_templates_xschem/pseudo_resistor_ctrl/pseudo_resistor_ctrl.sym} 1090 -850 0 0 {name=PRes
spiceprefix=X
}
C {devices/lab_pin.sym} 1130 -950 1 0 {name=l1 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1130 -880 3 0 {name=l2 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1100 -920 0 0 {name=l3 sig_type=std_logic lab=ref_in}
C {devices/lab_pin.sym} 1220 -900 0 1 {name=l4 sig_type=std_logic lab=inp}
C {devices/lab_pin.sym} 1220 -930 0 1 {name=l5 sig_type=std_logic lab=inn}
C {devices/lab_pin.sym} 1160 -970 1 0 {name=l6 sig_type=std_logic lab=CNT3}
C {pseudo_resistor_ctrl_gen/pseudo_resistor_ctrl_templates_xschem/pseudo_resistor_ctrl/pseudo_resistor_ctrl.sym} 1090 -600 0 0 {name=DUM_PRES
spiceprefix=X
}
C {devices/lab_pin.sym} 1130 -700 1 0 {name=l7 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1100 -670 0 0 {name=l8 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1220 -680 2 0 {name=l9 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1220 -650 2 0 {name=l10 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1160 -720 1 0 {name=l11 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1130 -630 3 0 {name=l12 sig_type=std_logic lab=VSS}
C {resistor_diff_gen/resistor_diff_templates_xschem/resistor_diff/resistor_diff.sym} 1380 -1160 1 1 {name=Res_diff1
spiceprefix=X
}
C {devices/lab_pin.sym} 1400 -1270 1 0 {name=l13 sig_type=std_logic lab=amp_vsip1}
C {devices/lab_pin.sym} 1430 -1270 1 0 {name=l14 sig_type=std_logic lab=amp_vsin1}
C {devices/lab_pin.sym} 1460 -1220 2 0 {name=l15 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1400 -1170 3 0 {name=l16 sig_type=std_logic lab=amp_vs1}
C {devices/lab_pin.sym} 1430 -1170 3 0 {name=l17 sig_type=std_logic lab=amp_vs1}
C {resistor_diff_gen/resistor_diff_templates_xschem/resistor_diff/resistor_diff.sym} 1580 -1160 1 1 {name=Res_diff2
spiceprefix=X
}
C {devices/lab_pin.sym} 1600 -1270 1 0 {name=l67 sig_type=std_logic lab=amp_vsip2}
C {devices/lab_pin.sym} 1630 -1270 1 0 {name=l68 sig_type=std_logic lab=amp_vsin2}
C {devices/lab_pin.sym} 1660 -1220 2 0 {name=l69 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 1600 -1170 3 0 {name=l70 sig_type=std_logic lab=amp_vs2}
C {devices/lab_pin.sym} 1630 -1170 3 0 {name=l71 sig_type=std_logic lab=amp_vs2}
C {cmfb_gen/cmfb_templates_xschem/cmfb/cmfb.sym} 340 -660 0 0 {name=CMFB
spiceprefix=X
}
C {devices/lab_pin.sym} 450 -900 1 0 {name=l72 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 450 -680 3 0 {name=l73 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 360 -770 2 1 {name=l74 sig_type=std_logic lab=outp}
C {devices/lab_pin.sym} 360 -840 2 1 {name=l75 sig_type=std_logic lab=outn}
C {devices/lab_pin.sym} 360 -800 2 1 {name=l76 sig_type=std_logic lab=vcm}
C {devices/lab_pin.sym} 360 -730 2 1 {name=l77 sig_type=std_logic lab=vcm}
C {devices/lab_pin.sym} 420 -900 1 0 {name=l78 sig_type=std_logic lab=cmfb_i}
C {devices/lab_pin.sym} 480 -900 1 0 {name=l79 sig_type=std_logic lab=amp_vs1}
C {devices/lab_pin.sym} 510 -900 1 0 {name=l80 sig_type=std_logic lab=amp_vs2}
C {devices/lab_pin.sym} 600 -790 2 0 {name=l82 sig_type=std_logic lab=out_loop}
C {devices/lab_pin.sym} 420 -680 3 0 {name=l81 sig_type=std_logic lab=en_CMFB}
C {devices/lab_pin.sym} 470 -680 3 0 {name=l83 sig_type=std_logic lab=amp_vsip1}
C {devices/lab_pin.sym} 490 -680 3 0 {name=l84 sig_type=std_logic lab=amp_vsin1}
C {devices/lab_pin.sym} 520 -680 3 0 {name=l85 sig_type=std_logic lab=amp_vsip2}
C {devices/lab_pin.sym} 540 -680 3 0 {name=l86 sig_type=std_logic lab=amp_vsin2}
C {devices/lab_pin.sym} 1530 -890 3 0 {name=l88 sig_type=std_logic lab=hvdd_out1}
C {devices/lab_pin.sym} 1530 -660 3 0 {name=l89 sig_type=std_logic lab=hvdd_out2}
C {devices/lab_pin.sym} 530 -900 1 0 {name=l87 sig_type=std_logic lab=NC}
C {devices/ipin.sym} 140 -980 0 0 {name=p16 lab=ref}
